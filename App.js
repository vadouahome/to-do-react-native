import {
  StyleSheet, SafeAreaView, FlatList, Text, View, Alert
} from 'react-native';
import Header from "./components/Header";
import {useState} from "react";
import ListItem from "./components/ListItem";
import Form from "./components/Form";


export default function App() {
  const [listItems, setListItems] = useState([
    {text: 'Добавте своє завдання', key: '1'},
  ])

  const addHandler = (x) => {
    if (x) {
      setListItems((e) => {
        return [
          {text: x, key: Math.random().toString(36).substring(7)},
          ...e
        ]
      })
    } else return Alert.alert('Увага!', 'Заповніть поле, тоді повторіть.', [{text: 'Добре'}])
  }

  const deleteHandler = (x) => {
    setListItems((e) => {
      return e.filter(el => el.key !== x)
    })
  }


  return (
    <SafeAreaView style={styles.container}>
      <Header/>

      <Form addHandler={addHandler}/>

      <View>
        <FlatList data={listItems} renderItem={({item}) => (
          <ListItem item={item} deleteHandler={deleteHandler}/>
        )}/>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(9,46,49)',
  }
});
