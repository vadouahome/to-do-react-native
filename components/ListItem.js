import {
  StyleSheet, Text, TouchableOpacity
} from 'react-native';


export default function ListItem({item, deleteHandler}) {
  return (
    <TouchableOpacity onPress={() => deleteHandler(item.key)}>
      <Text style={styles.text}>{item.text}</Text>
    </TouchableOpacity>
  );
}


const styles = StyleSheet.create({
  text: {
    width: '60%',
    marginTop: 30,
    marginLeft: '20%',
    padding: 20,
    borderRadius: 10,
    backgroundColor: 'rgb(6,86,91)',
    textAlign: 'center',
    color: '#fff',
    fontSize: 18,
  }
})
