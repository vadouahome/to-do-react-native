import {
  StyleSheet, View, Text, TextInput, Button, TouchableHighlight,
} from 'react-native';
import {useState} from "react";


export default function Form({addHandler}) {
  const [value, setValue] = useState('')

  const onChange = (x) => {
    setValue(x)
  }

  const handleOnPress = (x) => {
    addHandler(x)
    setValue('')
  }


  return (
    <View>
      <TextInput style={styles.input} onChangeText={onChange} placeholder={'Введіть завдання'} value={value}/>

      <Button title={'+'} onPress={() => handleOnPress(value)}/>
    </View>
  );
}


const styles = StyleSheet.create({
  input: {
    width: '60%',
    marginVertical: 50,
    marginHorizontal: '20%',
    padding: 20,
    borderWidth: 2,
    borderColor: 'rgba(16,144,155,1)',
    borderRadius: 10,
    color: '#fff',
  }
})
