import {
  StyleSheet, Text, View,
} from 'react-native';


export default function Header() {


  return (
    <View style={styles.main}>
      <Text style={styles.text}>Список справ</Text>
    </View>
  );
}


const styles = StyleSheet.create({
  main: {
    flex: .2,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 50,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 20,
    backgroundColor: 'rgb(6,86,91)',
  },
  text: {
    color: '#fff',
    fontSize: 18,
  }
})
